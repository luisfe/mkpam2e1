// Documento donde voy a definir mis pruebas

var expect = require('chai').expect; //Solo me interesa la parte de "expect" de toda la librería chai
var chai_jquery = require('chai-jquery'); //Biblioteca para poder recurrer nodos DOM
var request = require('request'); // Para hacer peticiones de URLs

describe("Pruebas sencillas", function() {
  it('Test suma', function() {   //Definición de una prueba
    expect(9+4).to.equal(13);
  });
});

describe("Pruebas red", function() {
  it('Test acceso internet', function(done) {   //Definición de prueba de conexion a internet. Es una prueba asíncrona por lo que tengo que definir un callback que le paso por parámetro
    request.get("http://www.atleticodemadrid.com",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
    });
  });

  it('Test acceso aplicacion puerto 8081', function(done) {   //Definición de prueba de acceso a mi aplicación por el puerto 8082
    request.get("http://localhost:8081/",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
    });
  });

  it('Test componente body', function(done) {   //Definición de prueba de acceso a mi aplicación por el puerto 8082
    request.get("http://localhost:8081/",
                function(error, response, body) {
                  expect(body).contains("<h1>Bienvenido a mi blog Jambo</h1>");
                  done();
    });
  });
});


describe("Pruebas contenido HTML", function() {
  it('Test componente h1', function(done) {   //Definición de prueba de acceso a mi aplicación por el puerto 8082
    request.get("http://localhost:8081/",
                function(error, response, body) {
                  expect($("body h1")).to.have.text("Bienvenido a mi blog Jambo");
                  done();
    });
  });
});
