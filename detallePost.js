function setPost(postId) {
  var posts = JSON.parse(sessionStorage["posts"]);
  var selectedPostId = postId;
  var elemento;

  var id = document.getElementById("lbId");
  var title = document.getElementById("lbTitle");
  var text = document.getElementById("lbText");
  var date = document.getElementById("lbDate");
  var author = document.getElementById("lbAuthor");

  for (var i = 0; i < posts.length; i++) {
    elemento = posts[i];
    if (elemento._id.$oid == selectedPostId) {
      break;
    }
    elemento = undefined;
  }

  if (elemento != undefined) {
      id.innerHTML = selectedPostId;
      title.innerHTML = elemento.titulo;
      text.innerHTML = elemento.texto;
      date.innerHTML = elemento.fecha;
      author.innerHTML = elemento.titulo;
      if (elemento.autor.apellido != undefined && elemento.autor.apellido != "N/A") {
        author.innerHTML = elemento.autor.nombre + " " + elemento.autor.apellido;
      }
      else {
        author.innerHTML = elemento.autor.nombre;
      }
  }
}
