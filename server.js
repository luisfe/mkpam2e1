'use strict';

const express = require('express'); //Variables que necesito
const path = require('path'); //Modulo que me permite obtener la ruta a donde tengo los archivos de mi contenedor

// Constantes
const PORT = 8081;

// App
const app = express(); //Inicializacion del servidor

app.use(express.static(__dirname)); //Asi se evita que busque en node_modules como dirección raíz.
app.get('/', function(req, res) {
  //res.send("<H1>Hola que ASE!!!\n</H1>");  //Primera version de mi servidor
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/detallePost/:id', function(req, res) {
  res.sendFile(path.join(__dirname + '/detallePost.html'));
});

app.get('/admin', function(req, res) {
  res.sendFile(path.join(__dirname + '/admin.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto " + PORT);
