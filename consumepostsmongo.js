const URL = "https://api.mlab.com/api/1/databases/tech-u-bootcamp/collections/posts?apiKey=cfC9AH4AebXQRi86oTVKVuP30-rd6jOb";
const apiKey = "?apiKey=cfC9AH4AebXQRi86oTVKVuP30-rd6jOb";
var response;

function getPosts() {

    var request = new XMLHttpRequest();
    request.open("GET", URL, false);   // (Tipo de operacion, URL a invocar, modo async [true] o sync [false])
    request.setRequestHeader("Content-Type", "application/json"); //Fijo la cabecera para ejecutar
    request.send();
    response = JSON.parse(request.responseText);
    sessionStorage["posts"] = request.responseText; //Comparto en el sessionStorage mi colección de POSTS para evitar volver a realizar peticiones no necesarias.
    console.log(response);

    //showPosts();
  }

function showPosts() {
  var table = document.getElementById("tablaPosts");
  for (var i = 0; i < response.length; i++) {
    var elemento = response[i];

    var fila = table.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaFecha = fila.insertCell(3);
    var celdaAutor = fila.insertCell(4);
    var celdaOperaciones = fila.insertCell(5);

    celdaId.innerHTML = elemento._id.$oid;
    celdaTitulo.innerHTML = elemento.titulo;
    celdaTexto.innerHTML = elemento.texto;
    celdaFecha.innerHTML = elemento.fecha;

    if (elemento.autor.apellido != undefined && elemento.autor.apellido != "N/A") {
      celdaAutor.innerHTML = elemento.autor.nombre + " " + elemento.autor.apellido;
    }
    else {
      celdaAutor.innerHTML = elemento.autor.nombre;
    }
    celdaOperaciones.innerHTML = '<button class="btn btn-secondary" type="button" onclick="modifyPost(\'' + celdaId.innerHTML + '\');">Actualizar elemento</button>';
  }
}

function addPost() {
  var request = new XMLHttpRequest();
  request.open("POST", URL, false);   // (Tipo de operacion, URL a invocar, modo async [true] o sync [false])
  request.setRequestHeader("Content-Type", "application/json"); //Fijo la cabecera para ejecutar
  request.send('{"titulo":"Nuevo POST desde ATOM", "texto":"Descripcion asociada a mi texto insertado desde ATOM", "autor":{"nombre":"Ron", "apellido":"Dennis"}, "fecha":"26/06/2017"}'); //El JSON lo especifico en el atributo de envío
}

function modifyPost(id) {
  var request = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/tech-u-bootcamp/collections/posts/";
  URLItem += id;
  URLItem += apiKey;
  request.open("PUT", URLItem, false);   // (Tipo de operacion, URL a invocar, modo async [true] o sync [false])
  request.setRequestHeader("Content-Type", "application/json"); //Fijo la cabecera para ejecutar
  request.send('{"titulo":"Nuevo POST modificado desde ATOM", "texto":"Descripcion asociada a mi texto modificado desde ATOM", "autor":{"nombre":"Ron", "apellido":"Dennis"}, "fecha":"26/06/2017"}'); //El JSON lo especifico en el atributo de envío
}

function setPostId(id) {
  sessionStorage["selectedPostId"] = JSON.parse(sessionStorage["posts"])[id]._id.$oid;
}
