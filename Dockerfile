FROM node:boron

#Crear directorio de la app
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Instalar las dependencias
COPY package.json /usr/src/app
RUN npm install

#Empaquetar codigo
COPY . /usr/src/app

#Expongo los puertos desde los que se atacara a mi contenedor
EXPOSE 8081

#Arranco mi aplicacion
CMD [ "npm", "start" ]
